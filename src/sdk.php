<?php

namespace MiParo;

use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Response;

class Sdk
{

    private $failures = [];
    private $successes = [];
    private $general_failure = [];
    //--------------------
    private $app_key;
    private $app_secret;
    private $api_base_url;
    private $endpoint;
    private $request_type;
    private $payload = [];

    public function __construct(Config $config)
    {

        $this->app_key = $config->app_key;
        $this->app_secret = $config->app_secret;
        $this->api_base_url = $config->base_url;

        /*  set_error_handler(function ($err_status, $error, $script, $line) {
              $this->report_error($this->get_response(),
                  $error . ' in ' . $script . ', line ' . $line
              );
          }, E_ALL);*/
    }

    /*  public function __destruct()
      {
          restore_error_handler();
      }*/

    public function send_request($isSyncRequest)
    {
        //erasing old result
        $this->successes = [];
        $this->failures = [];
        $this->general_failure = [];
        //processing request

        $packages = [];
        $package_num = 0;
        $cnt = 1;
        if (is_array($this->payload)) {
            if (!empty($this->payload)) {
                foreach ($this->payload as $k => $pl) {
                    if ($cnt > 10) {
                        $package_num++;
                        $cnt = 1;
                    }
                    $packages[$package_num][] = $pl;
                    $cnt++;
                }
            } else {
                $packages[0] = [];
            }
        } else {
            $packages[0] = $this->payload;
        }

        try {
            foreach ($packages as $package) {
                $request_data = [
                    "app_key" => $this->app_key,
                    "app_secret" => $this->app_secret,
                    "payload" => $package,
                    'is_sync_request' => $isSyncRequest
                ];

                $objectAsJson = json_encode($request_data);

                $ch = curl_init($this->api_base_url . $this->endpoint);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->request_type);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $objectAsJson);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 600);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);

                //execute request
                $result = curl_exec($ch);

                //close connection
                curl_close($ch);
                //print "Result: $result\n";
                $decoded = json_decode($result);

                if ($decoded) {
                    if ($decoded->result != 'General Failure') {
                        $this->successes = array_merge($this->successes, $decoded->details->successes);
                        $this->failures = array_merge($this->failures, $decoded->details->failures);
                    } else {
                        $this->successes = [];
                        $this->failures = [];
                        $this->general_failure = $decoded->details;
                        break;
                    }
                } else {
                    print_r($result);
                    // die(PHP_EOL);
                }
            }
        } catch (\Exception $e) {
            echo 'Error:' . $e . PHP_EOL;
            print_r($this->get_response());
            die(PHP_EOL);
        }
        
        $this->payload = [];
        return $this;
    }

    public function set_config(Config $config)
    {
        $this->app_key = $config->app_key;
        $this->app_secret = $config->app_secret;
        $this->api_base_url = $config->base_url;
        return $this;
    }

    /* public function set_request_type($request_type)
     {
         $this->request_type = $request_type;
         return $this;
     }

     public function set_payload($payload)
     {
         $this->payload = $payload;
         return $this;
     }*/

    public function request(RequestBuilder $requestBuilder, $isSyncRequest = false)
    {
        $this->endpoint = $requestBuilder->url;
        $this->payload = $requestBuilder->payload;
        $this->request_type = $requestBuilder->request_type;

        return $this->send_request($isSyncRequest)->get_response();
    }

    public function get_config()
    {
        return Config::create($this->app_key, $this->app_secret, $this->api_base_url);
    }

    public function get_request_type()
    {
        return $this->request_type;
    }

    public function get_payload()
    {
        return $this->payload;
    }

    public function get_endpoint()
    {
        return $this->endpoint;
    }

    public function get_response()
    {
        $mapper = new \JsonMapper();

        $pre_map = new \stdClass();

        $pre_map->successes = $this->successes;
        $pre_map->failures = $this->failures;
        $pre_map->general_failure = $this->general_failure;

        $response = $mapper->map($pre_map, new Response());

        return $response;

        /* return [
             'successes' => $this->successes,
             'failures' => $this->failures,
             'general_failure' => $this->general_failure
         ];*/

    }

    private function report_error($resp, $exception = false)
    {
        $request_data = [
            "app_key" => $this->app_key,
            "app_secret" => $this->app_secret,
            "payload" => [
                "app_key" => $this->app_key,
                "app_secret" => $this->app_secret,
                "sdk_config" => [
                    "base_url" => $this->api_base_url,
                    "request_type" => $this->get_request_type(),
                    "request_endpoint" => $this->get_endpoint(),
                    "request_payload" => $this->get_payload(),
                ],
                "server_response" => $resp,
                "server_env" => $_SERVER,
                "php_info" => self::parse_phpinfo(),
                "sdk_exception" => $exception,
                "sdk_response" => $this->get_response()
            ]
        ];

        $objectAsJson = json_encode($request_data);

        $ch = curl_init($this->api_base_url . 'error/report');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $objectAsJson);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);

        //execute request
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);

        $this->successes = [];
        $this->failures = [];
        $this->general_failure = "OOPS, something had happened. MiParo Team works hard to fix this issue!";

        return;
    }

    private static function parse_phpinfo()
    {
        ob_start();
        phpinfo(-1);
        $source = ob_get_clean();
        $pi = preg_replace(
            array('#^.*<body>(.*)</body>.*$#ms', '#<h2>PHP License</h2>.*$#ms',
                '#<h1>Configuration</h1>#', "#\r?\n#", "#</(h1|h2|h3|tr)>#", '# +<#',
                "#[ \t]+#", '#&nbsp;#', '#  +#', '# class=".*?"#', '%&#039;%',
                '#<tr>(?:.*?)" src="(?:.*?)=(.*?)" alt="PHP Logo" /></a>'
                . '<h1>PHP Version (.*?)</h1>(?:\n+?)</td></tr>#',
                '#<h1><a href="(?:.*?)\?=(.*?)">PHP Credits</a></h1>#',
                '#<tr>(?:.*?)" src="(?:.*?)=(.*?)"(?:.*?)Zend Engine (.*?),(?:.*?)</tr>#',
                "# +#", '#<tr>#', '#</tr>#'),
            array('$1', '', '', '', '</$1>' . "\n", '<', ' ', ' ', ' ', '', ' ',
                '<h2>PHP Configuration</h2>' . "\n" . '<tr><td>PHP Version</td><td>$2</td></tr>' .
                "\n" . '<tr><td>PHP Egg</td><td>$1</td></tr>',
                '<tr><td>PHP Credits Egg</td><td>$1</td></tr>',
                '<tr><td>Zend Engine</td><td>$2</td></tr>' . "\n" .
                '<tr><td>Zend Egg</td><td>$1</td></tr>', ' ', '%S%', '%E%'),
            $source);


        $sections = explode('<h2>', strip_tags($pi, '<h2><th><td>'));
        unset($sections[0]);

        //phpinfo => html given
        $pi = array();
        foreach ($sections as $section) {
            $n = substr($section, 0, strpos($section, '</h2>'));
            preg_match_all(
                '#%S%(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?%E%#',
                $section, $askapache, PREG_SET_ORDER);
            foreach ($askapache as $m)
                $pi[$n][$m[1]] = (!isset($m[3]) || @$m[2] == $m[3]) ? @$m[2] : array_slice($m, 2);
        }

        //phpinfo => cli lines
        if (empty($pi)) {
            $lines = explode("\n", $source);
            $cat = 'General';
            foreach ($lines as $k => $line) {
                if ($line == "PHP Variables") {
                    break;
                }
                if ($line == '') {
                    continue;
                }
                if ($line != '' && $lines[$k + 1] == '' && $lines[$k - 1] == '') {
                    $cat = trim($line);
                }

                $line_parts = explode('=>', $line);
                if (isset($line_parts[1])) {
                    $pi[$cat][trim($line_parts[0])]['local'] = trim($line_parts[1]);
                }
                if (isset($line_parts[2])) {
                    $pi[$cat][trim($line_parts[0])]['master'] = trim($line_parts[2]);
                }
            }
        }
        return $pi;
    }

}