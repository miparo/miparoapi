<?php

namespace MiParo\Blueprints\Interfaces;

interface PathWays
{

    const PATH_PROPERTY = 'property';

    const PATH_PROPERTY_ALL = 'property/all';
    const PATH_PROPERTY_FEATURES = 'property/features';
    const PATH_PROPERTY_LOCALISATION = 'property/localisation';
    const PATH_PROPERTY_IMAGES = 'property/images';
    const PATH_PROPERTY_PROXIMITIES = 'property/proximities';
    const PATH_PROPERTY_SOLD = 'property/sold';
    const PATH_PROPERTY_VIEWS = 'property/views';

    const PATH_AGENT = 'agent';
    const PATH_AGENT_ALL = 'agent/all';


}