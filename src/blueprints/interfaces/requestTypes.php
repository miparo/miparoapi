<?php

namespace MiParo\Blueprints\Interfaces;


interface RequestTypes
{
    const REQUEST_TYPE_GET = 'GET';
    const REQUEST_TYPE_POST = 'POST';
    const REQUEST_TYPE_PUT = 'PUT';
    const REQUEST_TYPE_PATCH = 'PATCH';
    const REQUEST_TYPE_DELETE = 'DELETE';
    const REQUEST_TYPE_COPY = 'COPY';
    const REQUEST_TYPE_HEAD = 'HEAD';
    const REQUEST_TYPE_OPTIONS = 'OPTIONS';
    const REQUEST_TYPE_LINK = 'LINK';
    const REQUEST_TYPE_UNLINK = 'UNLINK';
    const REQUEST_TYPE_PURGE = 'PURGE';
    const REQUEST_TYPE_LOCK = 'LOCK';
    const REQUEST_TYPE_UNLOCK = 'UNLOCK';
    const REQUEST_TYPE_PROPFIND = 'PROPFIND';
    const REQUEST_TYPE_VIEW = 'VIEW';

}