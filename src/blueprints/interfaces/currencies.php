<?php

namespace MiParo\Blueprints\Interfaces;


interface Currencies
{
    const EUR = 'EUR';
    const AED = 'AED';
    const USD = 'USD';
    const NOK = 'NOK';
    const CNY = 'CNY';
    const DKK = 'DKK';
    const INR = 'INR';
    const JPY = 'JPY';
    const RUB = 'RUB';
    const SEK = 'SEK';
    const UAH = 'UAH';

}