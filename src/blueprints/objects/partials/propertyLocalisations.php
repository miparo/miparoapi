<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Core\PropertyIdentifier;
use MiParo\Blueprints\Objects\Base\PropertyTexts as BaseLocalisations;


class PropertyLocalisations extends BaseLocalisations
{
    use PropertyIdentifier;

    /**
     * Instance of Base\PropertyTexts Object
     * @var ..\Base\PropertyTexts
     */
    public $texts;

    /**
     * PropertyLocalisations constructor.
     */
    public function __construct()
    {
        $this->texts = new BaseLocalisations();
    }

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

    /**
     * @param null $ext_id
     * @return $this
     */
    public static function create($ext_id = null)
    {
        return (new PropertyLocalisations())->fill($ext_id);
    }

}