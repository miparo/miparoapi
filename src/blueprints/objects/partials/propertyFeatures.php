<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Base\PropertyFeatures as BaseFeatures;
use MiParo\Blueprints\Objects\Core\PropertyIdentifier;

/**
 * Class PropertyFeatures
 * @package MiParo\Blueprints\Objects\Partials
 */
class PropertyFeatures extends BaseFeatures
{
    use PropertyIdentifier;

    /**
     * Instance of PropertyFeatures Object
     * @var ..\Base\PropertyFeatures
     */
    public $features;

    /**
     * PropertyFeatures constructor.
     */
    public function __construct()
    {
        $this->features = new BaseFeatures();
    }

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

    /**
     * @param null $ext_id
     * @return $this
     */
    public static function create($ext_id = null)
    {
        return (new PropertyFeatures())->fill($ext_id);
    }

}