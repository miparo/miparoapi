<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Base\PropertyViews as BaseViews;
use MiParo\Blueprints\Objects\Core\PropertyIdentifier;

/**
 * Class PropertyFeatures
 * @package MiParo\Blueprints\Objects\Partials
 */
class PropertyViews extends BaseViews
{
    use PropertyIdentifier;

    /**
     * Instance of PropertyFeatures Object
     * @var ..\Base\PropertyProximities
     */
    public $views;

    /**
     * PropertyFeatures constructor.
     */
    public function __construct()
    {
        $this->views = new BaseViews();
    }

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

    /**
     * @param null $ext_id
     * @return $this
     */
    public static function create($ext_id = null)
    {
        return (new PropertyViews())->fill($ext_id);
    }

}