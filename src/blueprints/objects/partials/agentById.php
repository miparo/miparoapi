<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Core\AgentIdentifier;

/**
 * Class AgentById
 * @package MiParo\Blueprints\Objects\Partials
 */
class AgentById
{
    use AgentIdentifier;

    /**
     * @param $email
     * @return $this
     */
    private function fill($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param null $email
     * @return $this
     */
    public static function create($email = null)
    {
        return (new AgentById())->fill($email);
    }
}