<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Core\PropertyIdentifier;


/**
 * Class PropertyById
 * @package MiParo\Blueprints\Objects\Partials
 */
class PropertyById
{
    use PropertyIdentifier;

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

    /**
     * @param null $ext_id
     * @return $this
     */
    public static function create($ext_id = null)
    {
        return (new PropertyById())->fill($ext_id);
    }
}