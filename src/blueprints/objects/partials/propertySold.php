<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Core\PropertyIdentifier;


/**
 * Class PropertySold
 * @package MiParo\Blueprints\Objects\Partials
 */
class PropertySold
{

    use PropertyIdentifier;

    /**
     * Property Sold Price
     * @var double
     */
    public $sold_price;

    /**
     * Property Sold Price Currency
     * @var string
     */
    public $sold_price_currency;

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

    /**
     * @param null $ext_id
     * @return $this
     */
    public static function create($ext_id = null)
    {
        return (new PropertySold())->fill($ext_id);
    }

}