<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Base\PropertyProximities as BaseProximities;
use MiParo\Blueprints\Objects\Core\PropertyIdentifier;

/**
 * Class PropertyFeatures
 * @package MiParo\Blueprints\Objects\Partials
 */
class PropertyProximities extends BaseProximities
{
    use PropertyIdentifier;

    /**
     * Instance of PropertyFeatures Object
     * @var ..\Base\PropertyProximities
     */
    public $proximities;

    /**
     * PropertyFeatures constructor.
     */
    public function __construct()
    {
        $this->proximities = new BaseProximities();
    }

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

    /**
     * @param null $ext_id
     * @return $this
     */
    public static function create($ext_id = null)
    {
        return (new PropertyProximities())->fill($ext_id);
    }

}