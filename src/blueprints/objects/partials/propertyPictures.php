<?php

namespace MiParo\Blueprints\Objects\Partials;

use MiParo\Blueprints\Objects\Core\PropertyIdentifier;
use MiParo\Blueprints\Objects\Base\PropertyPictures as BasePictures;

/**
 * Class PropertyPictures
 * @package MiParo\Blueprints\Objects\Partials
 */
class PropertyPictures
{
    use PropertyIdentifier;

    /**
     * @var bool
     */
    public $replace_all = false;

    /**
     * Array of instances of base Property Pictures object
     * @var BasePictures[]
     */
    public $images = array();

    /**
     * @param $ext_id
     * @return $this
     */
    public static function create($ext_id)
    {
        return (new PropertyPictures())->fill($ext_id);
    }

    /**
     * @param  BasePictures $picture
     * @return $this
     */
    public function add(BasePictures $picture)
    {
        $this->images[] = $picture;
        return $this;
    }

    /**
     * @param $ext_id
     * @return $this
     */
    private function fill($ext_id)
    {
        $this->external_id = $ext_id;
        return $this;
    }

}