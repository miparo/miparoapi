<?php


namespace MiParo\Blueprints\Objects;

use MiParo\Blueprints\Objects\Core\AgentIdentifier;

/**
 * Class Agent
 * @package MiParo\Blueprints\Objects
 */
class Agent
{
    use AgentIdentifier;

    /**
     * Agent's name
     * @var string|null
     */
    public $name;

    /**
     * Agent's profile picture
     * @var string|null
     */
    public $profile_picture;

    /**
     * Agent's phone number
     * @var string|null
     */
    public $phone;

    /**
     * Agent's cell phone number
     * @var string|null
     */
    public $mobile;

    /**
     * Agent's Skype id
     * @var string|null
     */
    public $skype;

    /**
     * Agent's brief bio
     * @var string|null
     */
    public $about_me;

    /**
     * Agent's twitter
     * @var string|null
     */
    public $twitter;

    /**
     * Agent's facebook
     * @var string|null
     */
    public $facebook;

    /**
     * Agent's pinterest
     * @var string|null
     */
    public $pinterest;


}