<?php

namespace MiParo\Blueprints\Objects\Core;


/**
 * Class PropertyIdentifier
 * @package MiParo\Blueprints\Objects\Core
 */
trait PropertyIdentifier
{

    /**
     * Property ID in your system
     * @var string
     * @required
     */
    public $external_id;

}