<?php

namespace MiParo\Blueprints\Objects\Core;

/**
 * Class AgentIdentifier
 * @package MiParo\Blueprints\Objects\Core
 */
trait AgentIdentifier
{

    /**
     * Agent's email
     * @var string
     * @required
     */
    public $email;

}