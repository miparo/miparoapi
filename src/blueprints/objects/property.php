<?php

namespace MiParo\Blueprints\Objects;

use MiParo\Blueprints\Objects\Core\PropertyIdentifier;
use MiParo\Blueprints\Objects\Base\PropertyTexts;
use MiParo\Blueprints\Objects\Base\PropertyPictures;
use MiParo\Blueprints\Objects\Base\PropertyFeatures;
use MiParo\Blueprints\Objects\Base\PropertyProximities;
use MiParo\Blueprints\Objects\Base\PropertyViews;


/**
 * Class Property
 * @package MiParo\Blueprints
 */
class Property
{
    use PropertyIdentifier;
    /**
     * Property Type (see docs to find out allowed values)
     * @var string
     */
    public $property_type;

    /**
     * Url to a property in your system
     * @var string
     */
    public $property_url;

    /**
     * Geographic Latitude
     * @var double
     */
    public $geo_lat;

    /**
     * Geographic Longitude
     * @var double
     */
    public $geo_lng;

    /**
     * Agent's email in MiParo system (see docs)
     * @var string
     * @required
     */
    public $agent_email;

    /**
     * Country where property is located
     * @var string
     */
    public $country;

    /**
     * State where property is located
     * @var string|null
     */
    public $state;

    /**
     * City where property is located
     * @var string|null
     */
    public $city;

    /**
     * Accurate address where property is located
     * @var string
     */
    public $street_address;

    /**
     * Number of rooms
     * @var integer
     */
    public $rooms;

    /**
     * Instance of Base\PropertyTexts Object
     * @var Base\PropertyTexts
     */
    public $texts;

    /**
     * Sign if property price is public value (see docs)
     * @var integer
     */
    public $price_on_request;

    /**
     * Property's sell price
     * @var double
     */
    public $sell_price_original;

    /**
     * Property's sell price currency in ISO 4217 format
     * https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     * @var string
     */
    public $sell_price_original_currency;

    /**
     * Property's monthly fee amount
     * @var double
     */
    public $monthly_fee;

    /**
     * Property's monthly fee currency in ISO 4217 format
     * https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     * @var string
     */
    public $monthly_fee_currency;

    /**
     * Property's rent amount
     * @var double
     */
    public $rent;

    /**
     * Property's rental currency in ISO 4217 format
     * https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     * @var string
     */
    public $rental_currency;

    /**
     * Property's rental monthly fee
     * @var double
     */
    public $rental_monthly_fee;

    /**
     * Property's rental monthly fee currency in ISO 4217 format
     * https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     * @var string
     */
    public $rental_monthly_fee_currency;

    /**
     * Property's rental duration (In Months)
     * @var integer
     */
    public $rental_duration;

    /**
     * Property tax amount
     * @var double
     */
    public $property_tax;

    /**
     * Property tax currency in ISO 4217 format
     * https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     * @var string
     */
    public $property_tax_currency;

    /**
     * Personal Property tax amount
     * @var double
     */
    public $personal_property_tax;

    /**
     * Personal property tax currency in SO 4217 format
     * https://en.wikipedia.org/wiki/ISO_4217#Active_codes
     * @var string
     */
    public $personal_property_tax_currency;

    /**
     * Year when property had been built in (YYYY) format
     * @var integer
     */
    public $build_year;

    /**
     * List of Instances of PropertyPictures Object
     * @var Base\PropertyPictures[]
     */
    public $property_images = array();

    /**
     * Instance of PropertyFeatures Object
     * @var Base\PropertyFeatures
     */
    public $features;

    /**
     * Instance of PropertyViews Object
     * @var Base\PropertyViews
     */
    public $views;

    /**
     * Instance of PropertyProximities Object
     * @var Base\PropertyProximities
     */
    public $proximities;

    /**
     * Total size of living area
     * @var double
     */
    public $total_living_area;

    /**
     * Living area measurement type: sq.m. or sq.ft.
     * @var string
     */
    public $total_living_area_type;

    /**
     * Total size of garden area
     * @var double
     */
    public $total_garden_area;

    /**
     * Living area measurement type: sq.m. or sq.ft.
     * @var string
     */
    public $total_garden_area_type;

    /**
     * Property constructor.
     */

     /**
     * Property preview mode
     * @var string
     */
    public $preview_mode;

    public function __construct()
    {
        $this->texts = new PropertyTexts();
        $this->features = new PropertyFeatures();
        $this->views = new PropertyViews();
        $this->proximities = new PropertyProximities();
    }

}
