<?php

namespace MiParo\Blueprints\Objects;

/**
 * Class Report
 * @package MiParo\Blueprints
 */
class Report
{

    /**
     * @var string|null
     */
    public $status;

    /**
     * @var string|null
     */
    public $message;

    /**
     * @var mixed|null
     */
    public $details;

}