<?php

namespace MiParo\Blueprints\Objects\Base;

/**
 * Class PropertyTexts
 * @package MiParo\Blueprints\Base
 */
class PropertyTexts
{
    /**
     * @var string|null
     */
    public $subject_bn;

    /**
     * @var string|null
     */
    public $subject_ro;

    /**
     * @var string|null
     */
    public $subject_fil;

    /**
     * @var string|null
     */
    public $subject_th;

    /**
     * @var string|null
     */
    public $subject_en;

    /**
     * @var string|null
     */
    public $subject_fr;

    /**
     * @var string|null
     */
    public $subject_de;

    /**
     * @var string|null
     */
    public $subject_sv;

    /**
     * @var string|null
     */
    public $subject_ar;

    /**
     * @var string|null
     */
    public $subject_no;

    /**
     * @var string|null
     */
    public $subject_es;

    /**
     * @var string|null
     */
    public $subject_es_mx;

    /**
     * @var string|null
     */
    public $subject_ma;

    /**
     * @var string|null
     */
    public $subject_da;

    /**
     * @var string|null
     */
    public $subject_fi;

    /**
     * @var string|null
     */
    public $subject_hi;

    /**
     * @var string|null
     */
    public $subject_pt;

    /**
     * @var string|null
     */
    public $subject_ru;

    /**
     * @var string|null
     */
    public $subject_ja;

    /**
     * @var string|null
     */
    public $subject_uk;

    /**
     * @var string|null
     */
    public $subject_it;

    /**
     * @var string|null
     */
    public $subject_ka;
    /**
     * @var string|null
     */
    public $description_bn;

    /**
     * @var string|null
     */
    public $description_ro;

    /**
     * @var string|null
     */
    public $description_fil;

    /**
     * @var string|null
     */
    public $description_th;

    /**
     * @var string|null
     */
    public $description_en;

    /**
     * @var string|null
     */
    public $description_fr;

    /**
     * @var string|null
     */
    public $description_de;

    /**
     * @var string|null
     */
    public $description_sv;

    /**
     * @var string|null
     */
    public $description_ar;

    /**
     * @var string|null
     */
    public $description_no;

    /**
     * @var string|null
     */
    public $description_es;

    /**
     * @var string|null
     */
    public $description_es_mx;

    /**
     * @var string|null
     */
    public $description_ma;

    /**
     * @var string|null
     */
    public $description_da;

    /**
     * @var string|null
     */
    public $description_fi;

    /**
     * @var string|null
     */
    public $description_hi;

    /**
     * @var string|null
     */
    public $description_pt;

    /**
     * @var string|null
     */
    public $description_ru;

    /**
     * @var string|null
     */
    public $description_ja;

    /**
     * @var string|null
     */
    public $description_uk;

    /**
     * @var string|null
     */
    public $description_it;

    /**
     * @var string|null
     */
    public $description_ka;

}
