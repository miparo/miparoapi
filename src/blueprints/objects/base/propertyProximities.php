<?php

namespace MiParo\Blueprints\Objects\Base;


/**
 * Class PropertyProximities
 * @package MiParo\Blueprints\Base
 */
class PropertyProximities
{
    /**
     * @var bool
     */
    public $airport = false;

    /**
     * @var bool
     */
    public $beach = false;

    /**
     * @var bool
     */
    public $golf = false;

    /**
     * @var bool
     */
    public $public_swimming_pool = false;

    /**
     * @var bool
     */
    public $public_transportation = false;

    /**
     * @var bool
     */
    public $park = false;

    /**
     * @var bool
     */
    public $sea = false;

    /**
     * @var bool
     */
    public $school = false;

    /**
     * @var bool
     */
    public $shopping = false;

    /**
     * @var bool
     */
    public $tennis = false;

    /**
     * @var bool
     */
    public $laundry = false;

    /**
     * @var bool
     */
    public $spa_hammam = false;

    /**
     * @var bool
     */
    public $cinema = false;

    /**
     * @var bool
     */
    public $supermarket = false;

    /**
     * @var bool
     */
    public $town_centre = false;

}

