<?php

namespace MiParo\Blueprints\Objects\Base;

/**
 * Class PropertyViews
 * @package MiParo\Blueprints\Base
 */
class PropertyViews
{
    /**
     * @var bool
     */
    public $city_view = false;

    /**
     * @var bool
     */
    public $garden_view = false;

    /**
     * @var bool
     */
    public $sea_view = false;

    /**
     * @var bool
     */
    public $courtyard_view = false;

}