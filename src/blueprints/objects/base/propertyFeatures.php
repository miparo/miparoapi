<?php

namespace MiParo\Blueprints\Objects\Base;


/**
 * Class PropertyFeatures
 * @package MiParo\Blueprints\Objects\Base
 */
class PropertyFeatures
{
    /**
     * @var bool
     */
    public $air_conditioning = false;

    /**
     * @var bool
     */
    public $attic = false;

    /**
     * @var bool
     */
    public $balcony = false;

    /**
     * @var bool
     */
    public $car_port = false;

    /**
     * @var bool
     */
    public $cellar = false;

    /**
     * @var bool
     */
    public $energy_efficient = false;

    /**
     * @var bool
     */
    public $fireplace = false;

    /**
     * @var bool
     */
    public $garage = false;

    /**
     * @var bool
     */
    public $garden = false;

    /**
     * @var bool
     */
    public $gated_community = false;

    /**
     * @var bool
     */
    public $jacuzzi = false;

    /**
     * @var bool
     */
    public $laundry = false;

    /**
     * @var bool
     */
    public $lift = false;

    /**
     * @var bool
     */
    public $master_bedroom = false;

    /**
     * @var bool
     */
    public $modern_exterior = false;

    /**
     * @var bool
     */
    public $modern_interior = false;

    /**
     * @var bool
     */
    public $open_floor_plan = false;

    /**
     * @var bool
     */
    public $patio = false;

    /**
     * @var bool
     */
    public $sauna = false;

    /**
     * @var bool
     */
    public $seashore = false;

    /**
     * @var bool
     */
    public $smart_home = false;

    /**
     * @var bool
     */
    public $swimming_pool = false;

    /**
     * @var bool
     */
    public $veranda = false;

    /**
     * @var bool
     */
    public $walk_in_closet = false;

    /**
     * @var bool
     */
    public $ski_in_ski_out = false;

}