<?php

namespace MiParo\Blueprints\Objects\Base;


/**
 * Class PropertyPictures
 * @package MiParo\Blueprints\Base
 */
class PropertyPictures
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var int
     */
    public $is_main;

    /**
     * @param string $url
     * @param int $is_main
     * @return PropertyPictures
     */
    public static function create($url, $is_main = 0)
    {
        return (new PropertyPictures())->fill($url, $is_main);
    }

    /**
     * @param string $url
     * @param int $is_main
     * @return PropertyPictures
     */
    private function fill($url, $is_main)
    {
        $this->url = $url;
        $this->is_main = $is_main;
        return $this;
    }

}