<?php

namespace MiParo\Blueprints;

use MiParo\Blueprints\Core\Pager;
use MiParo\Blueprints\Interfaces\PathWays;
use MiParo\Blueprints\Interfaces\RequestTypes;
use MiParo\Blueprints\Objects\Agent;
use MiParo\Blueprints\Objects\Partials\AgentById;
use MiParo\Blueprints\Objects\Property;

use MiParo\Blueprints\Objects\Partials\PropertyById;
use MiParo\Blueprints\Objects\Partials\PropertyFeatures;
use MiParo\Blueprints\Objects\Partials\PropertyProximities;
use MiParo\Blueprints\Objects\Partials\PropertyViews;
use MiParo\Blueprints\Objects\Partials\PropertyLocalisations;
use MiParo\Blueprints\Objects\Partials\PropertyPictures;
use MiParo\Blueprints\Objects\Partials\PropertySold;

class RequestBuilder implements RequestTypes, PathWays
{

    public $request_type = '';
    public $url = '';
    public $payload = [];

    const PROPERTY_ALL = [
        'request_type' => self::REQUEST_TYPE_POST,
        'url' => self::PATH_PROPERTY_ALL,
        'requires' => Pager::class
    ];

    /*const DELETE_PROPERTY_BY_ID = [
        'request_type' => self::REQUEST_TYPE_DELETE,
        'url' => self::PATH_PROPERTY,
        'requires' => [PropertyById::class]
    ];*/

    const PROPERTY_GET_BY_ID = [
        'request_type' => self::REQUEST_TYPE_POST,
        'url' => self::PATH_PROPERTY,
        'requires' => [PropertyById::class]
    ];

    const PROPERTY_FEATURES = [
        'request_type' => self::REQUEST_TYPE_PATCH,
        'url' => self::PATH_PROPERTY_FEATURES,
        'requires' => [PropertyFeatures::class]
    ];

    const PROPERTY_PROXIMITIES = [
        'request_type' => self::REQUEST_TYPE_PATCH,
        'url' => self::PATH_PROPERTY_PROXIMITIES,
        'requires' => [PropertyProximities::class]
    ];

    const PROPERTY_VIEWS = [
        'request_type' => self::REQUEST_TYPE_PATCH,
        'url' => self::PATH_PROPERTY_VIEWS,
        'requires' => [PropertyViews::class]
    ];

    const PROPERTY_PICTURES = [
        'request_type' => self::REQUEST_TYPE_PATCH,
        'url' => self::PATH_PROPERTY_IMAGES,
        'requires' => [PropertyPictures::class]
    ];

    const PROPERTY_LOCALISATIONS = [
        'request_type' => self::REQUEST_TYPE_PATCH,
        'url' => self::PATH_PROPERTY_LOCALISATION,
        'requires' => [PropertyLocalisations::class]
    ];

    const PROPERTY_SOLD = [
        'request_type' => self::REQUEST_TYPE_PATCH,
        'url' => self::PATH_PROPERTY_SOLD,
        'requires' => [PropertySold::class]
    ];

    const PROPERTY_CREATE_UPDATE = [
        'request_type' => self::REQUEST_TYPE_PUT,
        'url' => self::PATH_PROPERTY,
        'requires' => [Property::class]
    ];

    const AGENT_CREATE_UPDATE = [
        'request_type' => self::REQUEST_TYPE_PUT,
        'url' => self::PATH_AGENT,
        'requires' => [Agent::class]
    ];

    const AGENT_GET_BY_EMAIL = [
        'request_type' => self::REQUEST_TYPE_POST,
        'url' => self::PATH_AGENT,
        'requires' => [AgentById::class]
    ];

    const AGENT_ALL = [
        'request_type' => self::REQUEST_TYPE_POST,
        'url' => self::PATH_AGENT_ALL,
        'requires' => Pager::class
    ];

    const AGENT_DELETE_BY_EMAIL = [
        'request_type' => self::REQUEST_TYPE_DELETE,
        'url' => self::PATH_AGENT,
        'requires' => [AgentById::class]
    ];

    private function fill($request_type, $url, $payload)
    {
        $this->request_type = $request_type;
        $this->url = $url;
        $this->payload = $payload;
        return $this;
    }

    private static function validate($required, $given)
    {
        if (gettype(new $required) == gettype($given)) {

            if ($given instanceof $required) {
                $failure = false;

            } else {
                $failure = 'Payload has to be an instance of ' . get_class(new $required) . ', ' . get_class($given) . ' given!';
            }
        } else {
            $failure = 'Payload has to be Object, ' . gettype($given) . ' given!';
        }

        return $failure;
    }


    public static function build($request_config, $payload = false)
    {
        // if no validation required, nothing can happen
        $failure = false;
        //validate payload
        if ($request_config['requires']) {
            // payload is required for this request, possible unexpected error might happen
            $failure = 'Unknown SDK error occurred!';
            if (is_array($request_config['requires'])) {
                if (is_array($payload) && !empty($payload)) {
                    foreach ($payload as $p) {
                        $test = self::validate($request_config['requires'][0], $p);
                        if ($test) {
                            $failure = 'One or more items in Payload array is not an instance of ' . get_class(new $request_config['requires'][0]);
                            break;
                        } else {
                            $failure = false;
                        }
                    }
                } else {
                    $failure = 'Payload has to be an array of ' . get_class(new $request_config['requires'][0]) . ' instances, ' . gettype($payload) . ' given!';
                }
            } else {
                $failure = self::validate($request_config['requires'], $payload);
            }

            if ($failure) {
                throw new \InvalidArgumentException($failure);
            }
        }

        if (!$failure) {
            return (new RequestBuilder())->fill($request_config['request_type'], $request_config['url'], $payload);
        } else {
            // Exception will be thrown
        }

    }

}