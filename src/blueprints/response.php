<?php

namespace MiParo\Blueprints;

use JsonMapper;
use MiParo\Blueprints\Objects\Property;
use MiParo\Blueprints\Objects\Agent;
use MiParo\Blueprints\Objects\Report;

/**
 * Class Response
 * @package MiParo\Blueprints
 */
class Response
{

    /**
     * @var mixed[]|null
     */
    public $successes;

    /**
     * @var Objects\Report[]|null
     */
    public $failures;

    /**
     * @var array|null
     */
    public $general_failure;

    public function setSuccesses($successes)
    {
        $mapper = new JsonMapper();
        foreach ($successes as $s) {

            if (isset($s->status) && isset($s->message)) {
                $this->successes[] = $mapper->map($s, new Report());
            } elseif (isset($s->external_id)) {
                $this->successes[] = $mapper->map($s, new Property());
            } elseif (isset($s->email)) {
                $this->successes[] = $mapper->map($s, new Agent());
            }

        }
    }


}