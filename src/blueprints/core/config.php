<?php

namespace MiParo\Blueprints\Core;


/**
 * Class Config
 * @package MiParo\Blueprints\Core
 */
class Config
{

    /**
     * @var string
     */
    public $app_key;

    /**
     * @var string
     */
    public $app_secret;

    /**
     * @var string
     */
    public $base_url;

    /**
     * @param string $key
     * @param string $secret
     * @param string $url
     * @return $this
     */
    private function fill($key, $secret, $url)
    {
        $this->app_key = $key;
        $this->app_secret = $secret;
        $this->base_url = $url;
        return $this;
    }

    /**
     * @param string $key
     * @param string $secret
     * @param string|null $url
     * @return $this
     */
    public static function create($key, $secret, $url = 'http://miparo.com/api/')
    {

        return (new Config())->fill($key, $secret, $url);

    }

}