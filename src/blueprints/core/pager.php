<?php
namespace MiParo\Blueprints\Core;


/**
 * Class Pager
 * @package MiParo\Blueprints\Core
 */
class Pager
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @param int $page
     * @return $this
     */
    private function fill($page = 1)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int $page
     * @return $this
     */
    public static function create($page = 1)
    {
        return (new Pager())->fill($page);
    }

}