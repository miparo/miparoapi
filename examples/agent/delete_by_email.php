<?php
require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Objects\Partials\AgentById;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$payload = array();

$payload[] = AgentById::create('some1@email.dom');
$payload[] = AgentById::create('fake_email@email.com');
$payload[] = AgentById::create('wrong@email@com');

$response = $api->request(RequestBuilder::build(RequestBuilder::AGENT_DELETE_BY_EMAIL, $payload));

print_r($response);

die(PHP_EOL);