<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Pager;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$response = $api->request(RequestBuilder::build(RequestBuilder::AGENT_ALL, Pager::create(3)));

print_r($response);

die(PHP_EOL);