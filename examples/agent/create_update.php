<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Objects\Agent;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;


$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$payload = array();

$agent = new Agent();

$agent->name = 'Test User 1';
$agent->email = 'some1@email.dom';
$agent->phone = '+12 3 45 67 89 01';
$agent->mobile = '+12 3 45 67 89 01';
$agent->skype = 'some_skype_id';
$agent->about_me = 'I\'m just a test user, never mind me.';
$agent->twitter = 'some_twitter_id';
$agent->facebook = 'some_facebook_id';
$agent->pinterest = 'some_pinterest_id';

$payload[] = $agent;
/* you might add as many Agents as you need to payload */

$response = $api->request(RequestBuilder::build(RequestBuilder::AGENT_CREATE_UPDATE, $payload));

print_r($response);

die(PHP_EOL);