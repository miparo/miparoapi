<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Objects\Partials\PropertyPictures;
use MiParo\Blueprints\Objects\Base\PropertyPictures as BasePictures;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$payload = [];

$property_image = PropertyPictures::create(md5(123));
$property_image->replace_all = true;
$property_image->add(BasePictures::create('https://miparo.com/assets/img/logo.png', 1));
$property_image->add(BasePictures::create('https://miparo.com/assets/img/logo.png'));

$payload[] = $property_image;
//  you might add as many Property Pictures as you need to payload

$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_PICTURES, $payload));

print_r($response);

die(PHP_EOL);