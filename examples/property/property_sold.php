<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Objects\Partials\PropertySold;
use MiParo\Blueprints\Interfaces\Currencies;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$payload = [];

$property_sold = PropertySold::create(md5(123));

$property_sold->sold_price = 300000;
$property_sold->sold_price_currency = Currencies::SEK;

$payload[] = $property_sold;
//  you might add as many Property Sold objects as you need to payload

$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_SOLD, $payload));

print_r($response);

die(PHP_EOL);