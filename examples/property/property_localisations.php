<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Objects\Partials\PropertyLocalisations;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));


$localisation = PropertyLocalisations::create(md5(123));

$localisation->texts->subject_ar = 'Oh, really? it\'s not arabic subject!';
$localisation->texts->description_ar = 'Oh, really? it\'s not arabic description!';

$payload = [];

$payload[] = $localisation;

$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_LOCALISATIONS, $payload));

print_r($response);

die(PHP_EOL);