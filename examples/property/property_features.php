<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Objects\Partials\PropertyFeatures;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$property_features = PropertyFeatures::create(md5(123));

$payload = [];

$property_features->features->attic = true;
$property_features->features->car_port = true;
$property_features->features->energy_efficient = true;
$property_features->features->cellar = true;
$property_features->features->jacuzzi = true;
$property_features->features->garage = true;

$payload[] = $property_features;
//  you might add as many Property Features as you need to payload

$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_FEATURES, $payload));

print_r($response);

die(PHP_EOL);