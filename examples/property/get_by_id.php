<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Objects\Partials\PropertyById;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$payload = array();

$payload[] = PropertyById::create(md5(123));
$payload[] = PropertyById::create(md5(1234));
$payload[] = PropertyById::create(md5(12345));
$payload[] = PropertyById::create(md5(1234567));
$payload[] = PropertyById::create(md5(12345678));


$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_GET_BY_ID, $payload));

print_r($response);

die(PHP_EOL);