<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Objects\Property;
use MiParo\Blueprints\Objects\Base\PropertyPictures;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Interfaces\Currencies;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$property = new Property();

$property->external_id = md5(123);
$property->property_type = 'house';
$property->agent_email = 'johan@wretmanestate.com';
$property->property_url = 'http://www.yourrealestate.domain/property/url.html';
$property->price_on_request = 0;
$property->sell_price_original = 1750000;
$property->sell_price_original_currency = Currencies::EUR;
$property->property_tax = 0;
$property->property_tax_currency = Currencies::USD;
$property->personal_property_tax = 0;
$property->personal_property_tax_currency = Currencies::SEK;
$property->monthly_fee = 1500;
$property->monthly_fee_currency = Currencies::EUR;
$property->total_living_area = 1000;
$property->total_living_area_type = 'sq.m.';
$property->total_garden_area = 10000;
$property->total_garden_area_type = 'sq.m.';
$property->country = 'Sweden';
$property->rooms = 7;
$property->build_year = 2014;

$property->texts->subject_en = 'Beautiful villa - Vence';
$property->texts->description_en = 'Some Cool description!';

$property->features->air_conditioning = 1;
$property->features->attic = 4;

$property->proximities->airport = true;
$property->proximities->sea = true;

$property->views->sea_view = true;

$property->property_images[] = PropertyPictures::create("https://www.google.com/logos/doodles/2017/st-patricks-day-2017-5654938628653056-res.png", 0);
$property->property_images[] = PropertyPictures::create("http://www.miparo.com/assets/img/logo.png", 1);

$payload = array();
$payload[] = $property;
/* you might add as many Properties as you need to payload */

$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_CREATE_UPDATE, $payload));

print_r($response);

die(PHP_EOL);