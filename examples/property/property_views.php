<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use MiParo\Sdk;
use MiParo\Blueprints\Core\Config;
use MiParo\Blueprints\RequestBuilder;
use MiParo\Blueprints\Objects\Partials\PropertyViews;

$api = new Sdk(Config::create('97c3322e3659b2a925b7ba4475f7054e', '7d92c68fe51f6af2e3caec70a88061c9b4e58ea1', 'http://miparo.loc/api/'));

$property_views = PropertyViews::create(md5(123));

$payload = [];

$property_views->views->sea_view = true;
$property_views->views->garden_view = true;
$payload[] = $property_views;
//  you might add as many Property Proximities as you need to payload

$response = $api->request(RequestBuilder::build(RequestBuilder::PROPERTY_VIEWS, $payload));

print_r($response);

die(PHP_EOL);